import ResourceService from '@/services/ResourceService'
import Message from '@/models/Message'
import MessageRequest from '@/models/MessageRequest'

export default class MessageService extends ResourceService {
  static async retrieve (id: string, namespaceId: string | null): Promise<Message> {
    return this
      ._retrieve<Message>(this.path(namespaceId, id))
      .catch(this.rejectWithError('error retrieving message'))
  }

  static async retrieveAll (namespaceId: string | null, filter: string | null): Promise<Array<Message>> {
    let path = this.path(namespaceId, null)

    if (filter) {
      path += `?$filter=${encodeURI(filter)}`
    }

    return this
      ._retrieveAll<Array<Message>>(path)
      .catch(this.rejectWithError('error retrieving messages'))
  }

  static async create (message: MessageRequest, namespaceId: string | null): Promise<Message> {
    return this
      ._create<Message>(this.path(namespaceId, null), message)
      .catch(this.rejectWithError('error creating message'))
  }

  static path (namespaceId: string | null, id: string | null): string {
    let path = ''

    if (namespaceId) {
      path += `/namespaces/${namespaceId}`
    }

    path += '/messages'

    if (id) {
      path += `/${id}`
    }

    return path
  }
}
