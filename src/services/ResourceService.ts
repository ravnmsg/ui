import axios, { AxiosError } from 'axios'

import ResponseEnvelope from '@/services/ResponseEnvelope'

export default class ResourceService {
  static async _retrieve<R> (path: string): Promise<R> {
    return axios
      .get<ResponseEnvelope<R>>(
        this.buildUri(path),
        {
          headers: {
            user: 'admin'
          }
        })
      .then((resp) => { return resp.data })
      .then(this.validateResponse())
      .then(this.validateResponseData())
      .catch(this.handleError())
  }

  // TODO: add query
  static async _retrieveAll<R> (path: string): Promise<R> {
    return axios
      .get<ResponseEnvelope<R>>(
        this.buildUri(path),
        {
          headers: {
            Accept: 'application/json',
            user: 'admin'
          }
        })
      .then((resp) => { return resp.data })
      .then(this.validateResponse())
      .then(this.validateResponseData())
      .catch(this.handleError())
  }

  static async _create<R> (path: string, resource: unknown): Promise<R> {
    return axios
      .post<ResponseEnvelope<R>>(
        this.buildUri(path),
        resource,
        {
          headers: {
            user: 'admin'
          }
        })
      .then((resp) => { return resp.data })
      .then(this.validateResponse())
      .then(this.validateResponseData())
      .catch(this.handleError())
  }

  static async _update<R> (path: string, resource: unknown): Promise<R> {
    return axios
      .put<ResponseEnvelope<R>>(
        this.buildUri(path),
        resource,
        {
          headers: {
            user: 'admin'
          }
        })
      .then((resp) => { return resp.data })
      .then(this.validateResponse())
      .then(this.validateResponseData())
      .catch(this.handleError())
  }

  static async _delete<R> (path: string): Promise<boolean> {
    return axios
      .delete<ResponseEnvelope<R>>(
        this.buildUri(path),
        {
          headers: {
            user: 'admin'
          }
        })
      .then((resp) => { return resp.data })
      .then(this.validateResponse())
      .then(() => { return true })
      .catch(this.handleError())
  }

  static validateResponse<R = unknown> (): (response: ResponseEnvelope<R>) => Promise<ResponseEnvelope<R>> {
    return (response: ResponseEnvelope<R>) => {
      if (response.errors) {
        return Promise.reject(new Error(response.errors.shift() || 'error'))
      }

      return Promise.resolve(response)
    }
  }

  static validateResponseData<R = unknown> (): (response: ResponseEnvelope<R>) => Promise<R> {
    return (response: ResponseEnvelope<R>) => {
      if (!response.data) {
        return Promise.reject(new Error('response data is empty'))
      }

      return Promise.resolve(response.data)
    }
  }

  static handleError (): (error: AxiosError) => Promise<never> {
    return (error) => {
      let msg
      if (error.response) {
        const response = error.response.data as ResponseEnvelope<unknown>
        if (response.errors && response.errors.length > 0) {
          msg = response.errors.shift()
        } else {
          msg = 'error with response: ' + error.message
        }
      } else if (error.request) {
        msg = 'error with request: ' + error.message
      } else {
        msg = error.message
      }

      return Promise.reject(new Error(msg))
    }
  }

  static rejectWithError (errorMessage: string): (reason: Error) => Promise<never> {
    return (error: Error) => {
      error = new Error(errorMessage + ': ' + error.message)
      return Promise.reject(error)
    }
  }

  static buildUri (path: string) {
    const host = process.env.VUE_APP_RAVN_RECEIVER_HOST
    return `http://${host}/v1${path}`
  }
}
