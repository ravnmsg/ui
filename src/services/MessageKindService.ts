import ResourceService from '@/services/ResourceService'
import MessageKind from '@/models/MessageKind'

export default class MessageKindService extends ResourceService {
  static async retrieve (id: string, namespaceId: string | null): Promise<MessageKind> {
    return this
      ._retrieve<MessageKind>(this.path(namespaceId, id))
      .catch(this.rejectWithError('error retrieving message kind'))
  }

  static async retrieveAll (namespaceId: string | null, filter: string | null): Promise<Array<MessageKind>> {
    let path = this.path(namespaceId, null)

    if (filter) {
      path += `?$filter=${encodeURI(filter)}`
    }

    return this
      ._retrieveAll<Array<MessageKind>>(path)
      .catch(this.rejectWithError('error retrieving message kinds'))
  }

  static async create (messageKind: MessageKind, namespaceId: string | null): Promise<MessageKind> {
    return this
      ._create<MessageKind>(this.path(namespaceId, null), messageKind)
      .catch(this.rejectWithError('error creating message kind'))
  }

  static async update (id: string, messageKind: MessageKind, namespaceId: string | null): Promise<MessageKind> {
    return this
      ._update<MessageKind>(this.path(namespaceId, id), messageKind)
      .catch(this.rejectWithError('error updating message kind'))
  }

  static async delete (id: string, namespaceId: string | null): Promise<boolean> {
    return this
      ._delete<MessageKind>(this.path(namespaceId, id))
      .catch(this.rejectWithError('error deleting message kind'))
  }

  static path (namespaceId: string | null, id: string | null): string {
    let path = ''

    if (namespaceId) {
      path += `/namespaces/${namespaceId}`
    }

    path += '/messageKinds'

    if (id) {
      path += `/${id}`
    }

    return path
  }
}
