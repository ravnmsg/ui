import ResourceService from '@/services/ResourceService'
import Template from '@/models/Template'

export default class TemplateService extends ResourceService {
  static async retrieve (id: string, namespaceId: string | null): Promise<Template> {
    return this
      ._retrieve<Template>(this.path(namespaceId, id))
      .catch(this.rejectWithError('error retrieving template'))
  }

  static async retrieveAll (namespaceId: string | null, filter: string | null): Promise<Array<Template>> {
    let path = this.path(namespaceId, null)

    if (filter) {
      path += `?$filter=${encodeURI(filter)}`
    }

    return this
      ._retrieveAll<Array<Template>>(path)
      .catch(this.rejectWithError('error retrieving templates'))
  }

  static async create (template: Template, namespaceId: string | null): Promise<Template> {
    return this
      ._create<Template>(this.path(namespaceId, null), template)
      .catch(this.rejectWithError('error creating template'))
  }

  static async update (id: string, template: Template, namespaceId: string | null): Promise<Template> {
    return this
      ._update<Template>(this.path(namespaceId, id), template)
      .catch(this.rejectWithError('error updating template'))
  }

  static async delete (id: string, namespaceId: string | null): Promise<boolean> {
    return this
      ._delete<Template>(this.path(namespaceId, id))
      .catch(this.rejectWithError('error deleting template'))
  }

  static path (namespaceId: string | null, id: string | null): string {
    let path = ''

    if (namespaceId) {
      path += `/namespaces/${namespaceId}`
    }

    path += '/templates'

    if (id) {
      path += `/${id}`
    }

    return path
  }
}
