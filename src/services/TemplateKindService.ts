import ResourceService from '@/services/ResourceService'
import TemplateKind from '@/models/TemplateKind'

export default class TemplateKindService extends ResourceService {
  static async retrieve (id: string): Promise<TemplateKind> {
    return this
      ._retrieve<TemplateKind>(this.path(id))
      .catch(this.rejectWithError('error retrieving template kind'))
  }

  static async retrieveAll (): Promise<Array<TemplateKind>> {
    return this
      ._retrieveAll<Array<TemplateKind>>(this.path(null))
      .catch(this.rejectWithError('error retrieving template kinds'))
  }

  static async create (templateKind: TemplateKind): Promise<TemplateKind> {
    return this
      ._create<TemplateKind>(this.path(null), templateKind)
      .catch(this.rejectWithError('error creating template kind'))
  }

  static async update (id: string, templateKind: TemplateKind): Promise<TemplateKind> {
    return this
      ._update<TemplateKind>(this.path(id), templateKind)
      .catch(this.rejectWithError('error updating template kind'))
  }

  static async delete (id: string): Promise<boolean> {
    return this
      ._delete<TemplateKind>(this.path(id))
      .catch(this.rejectWithError('error deleting template kind'))
  }

  static path (id: string | null): string {
    let path = '/templateKinds'

    if (id) {
      path += `/${id}`
    }

    return path
  }
}
