import ResourceService from '@/services/ResourceService'
import Namespace from '@/models/Namespace'

export default class NamespaceService extends ResourceService {
  static async retrieve (id: string): Promise<Namespace> {
    return this
      ._retrieve<Namespace>(this.path(id))
      .catch(this.rejectWithError('error retrieving namespace'))
  }

  static async retrieveAll (): Promise<Array<Namespace>> {
    return this
      ._retrieveAll<Array<Namespace>>(this.path(null))
      .catch(this.rejectWithError('error retrieving namespaces'))
  }

  static async create (namespace: Namespace): Promise<Namespace> {
    return this
      ._create<Namespace>(this.path(null), namespace)
      .catch(this.rejectWithError('error creating namespace'))
  }

  static async update (id: string, namespace: Namespace): Promise<Namespace> {
    return this
      ._update<Namespace>(this.path(id), namespace)
      .catch(this.rejectWithError('error updating namespace'))
  }

  static async delete (id: string): Promise<boolean> {
    return this
      ._delete<Namespace>(this.path(id))
      .catch(this.rejectWithError('error deleting namespace'))
  }

  static path (id: string | null): string {
    let path = '/namespaces'

    if (id) {
      path += `/${id}`
    }

    return path
  }
}
