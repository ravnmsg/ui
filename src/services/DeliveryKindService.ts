import ResourceService from '@/services/ResourceService'
import DeliveryKind from '@/models/DeliveryKind'

export default class DeliveryKindService extends ResourceService {
  static async retrieve (id: string): Promise<DeliveryKind> {
    return this
      ._retrieve<DeliveryKind>(this.path(id))
      .catch(this.rejectWithError('error retrieving delivery kind'))
  }

  static async retrieveAll (): Promise<Array<DeliveryKind>> {
    return this
      ._retrieveAll<Array<DeliveryKind>>(this.path(null))
      .catch(this.rejectWithError('error retrieving delivery kinds'))
  }

  static async create (deliveryKind: DeliveryKind): Promise<DeliveryKind> {
    return this
      ._create<DeliveryKind>(this.path(null), deliveryKind)
      .catch(this.rejectWithError('error creating delivery kind'))
  }

  static async update (id: string, deliveryKind: DeliveryKind): Promise<DeliveryKind> {
    return this
      ._update<DeliveryKind>(this.path(id), deliveryKind)
      .catch(this.rejectWithError('error updating delivery kind'))
  }

  static async delete (id: string): Promise<boolean> {
    return this
      ._delete<DeliveryKind>(this.path(id))
      .catch(this.rejectWithError('error deleting delivery kind'))
  }

  static path (id: string | null): string {
    let path = '/deliveryKinds'

    if (id) {
      path += `/${id}`
    }

    return path
  }
}
