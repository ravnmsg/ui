export default interface ResponseEnvelope<R> {
    data?: R;
    errors?: Array<string>;
    meta: Record<string, unknown>;
}
