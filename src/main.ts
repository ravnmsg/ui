import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

createApp(App)
  .use(store)
  .use(router)
  .mount('#app')

// new Vue(App).$mount('#app')

// Vue.createApp({})
//   .component('SearchInput', SearchInputComponent)
//   .directive('focus', FocusDirective)
//   .use(LocalePlugin)

// const RootComponent = { /* options */ }
// const app = Vue.createApp(RootComponent)
// const vm = app.mount('#app')
