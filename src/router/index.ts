import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import DeliveryKinds from '../views/DeliveryKinds.vue'
import Messages from '../views/Messages.vue'
import MessageKinds from '../views/MessageKinds.vue'
import MessageKind from '../views/MessageKind.vue'
import Namespaces from '../views/Namespaces.vue'
import Namespace from '../views/Namespace.vue'
import Templates from '../views/Templates.vue'
import TemplateKinds from '../views/TemplateKinds.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }, {
    path: '/deliveryKinds',
    name: 'deliveryKinds',
    component: DeliveryKinds
  }, {
    path: '/messages',
    name: 'messages',
    component: Messages
  }, {
    path: '/messageKinds',
    name: 'messageKinds',
    component: MessageKinds
  }, {
    path: '/namespaces/:namespaceId/messageKinds/:id',
    name: 'messageKind',
    component: MessageKind,
    props: true
  }, {
    path: '/namespaces',
    name: 'namespaces',
    component: Namespaces
  }, {
    path: '/namespaces/:id',
    name: 'namespace',
    component: Namespace,
    props: true
  }, {
    path: '/templates',
    name: 'templates',
    component: Templates
  }, {
    path: '/templateKinds',
    name: 'templateKinds',
    component: TemplateKinds
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
