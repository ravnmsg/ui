export default interface Namespace {
  id: string;
  code: string;
  createTime: Date;
  updateTime: Date;
}
