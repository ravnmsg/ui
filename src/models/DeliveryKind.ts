export default interface DeliveryKind {
  id: string;
  code: string;
  host: string;
  disabled: boolean;
  createTime: Date;
  updateTime: Date;
}
