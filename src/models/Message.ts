import Delivery from '@/models/Delivery'

export default interface Message {
  id: string;
  messageKind: string;
  languageTag: string;
  input: Map<string, unknown>;
  createTime: Date;
  deliveries: Array<Delivery>;
}
