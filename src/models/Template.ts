export default interface Template {
  id: string;
  templateKindId: string;
  messageKindId: string;
  languageTag: boolean;
  body: string;
  createTime: Date;
  updateTime: Date;
}
