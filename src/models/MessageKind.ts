export default interface MessageKind {
  id: string;
  code: string;
  namespaceId: string;
  disabled: boolean;
  createTime: Date;
  updateTime: Date;
}
