export default interface TemplateKind {
  id: string;
  deliveryKindId: string;
  targetAttr: string;
  createTime: Date;
  updateTime: Date;
}
