export default interface Delivery {
  deliveryKind: string;
  status: string;
  statusMessage: string;
  scheduleTime?: Date;
  deliverTime?: Date;
}
