export default interface MessageRequest {
  messageKind: string;
  languageTag: string;
  deliveryKind: string;
  input: Map<string, unknown>;
}
